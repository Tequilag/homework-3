//
//  InverseOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 10.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "InverseOperation.h"

@implementation InverseOperation

-(double)perform:(double)value{
    return 1.0/value;
}
@end
