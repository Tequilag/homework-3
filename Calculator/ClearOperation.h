//
//  ClearOperation.h
//  Calculator
//
//  Created by Gorbenko Georgy on 09.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClearOperation : NSObject

-(double)clearNumber:(double)value;
@end
