//
//  multiOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 10.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "multiOperation.h"

@implementation MultiOperation

-(double)performWithSecondArgument:(double)secondArgument{
    return self.firstArgument*secondArgument;
}
@end
