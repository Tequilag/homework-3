//
//  CosOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 10.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "CosOperation.h"

@implementation CosOperation

-(double)perform:(double)value{
    return cos(value);
}
@end
