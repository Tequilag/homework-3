//
//  PartitionOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 10.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "PartitionOperation.h"

@implementation PartitionOperation

-(double)performWithSecondArgument:(double)secondArgument{
    return self.firstArgument/secondArgument;
}
@end
