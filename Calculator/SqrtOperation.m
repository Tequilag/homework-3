//
//  SqrtOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 10.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "SqrtOperation.h"

@implementation SqrtOperation

-(double)perform:(double)value{
    return sqrt(value);
}
@end
