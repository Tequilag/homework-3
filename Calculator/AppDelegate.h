//
//  AppDelegate.h
//  Calculator
//
//  Created by Gorbenko Georgy on 08.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

