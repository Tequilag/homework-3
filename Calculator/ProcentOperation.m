//
//  ProcentOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 10.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "ProcentOperation.h"

@implementation ProcentOperation

-(double)perform:(double)value{
    return value/100;
}
@end
