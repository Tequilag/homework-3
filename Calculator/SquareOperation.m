//
//  SquareOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 11.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "SquareOperation.h"

@implementation SquareOperation

-(double)perform:(double)value{
    return value*value;
}
@end
