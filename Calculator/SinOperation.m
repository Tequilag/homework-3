//
//  SinOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 10.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "SinOperation.h"

@implementation SinOperation

-(double) perform:(double)value{
    return sin(value);
}
@end
