//
//  ClearInputOperation.h
//  Calculator
//
//  Created by Gorbenko Georgy on 09.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClearInputOperation : NSObject

-(double)clearInputNumber:(double)value;
@end
