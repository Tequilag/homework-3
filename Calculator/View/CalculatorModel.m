//
//  CalculatorModel.m
//  Calculator
//
//  Created by Gorbenko Georgy on 09.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "CalculatorModel.h"
#import "SumOperation.h"
#import "SignOperation.h"
#import "EqualOperation.h"
#import "ClearOperation.h"
#import "ClearInputOperation.h"
#import "MinusOperation.h"
#import "MultiOperation.h"
#import "PartitionOperation.h"
#import "ProcentOperation.h"
#import "SquareOperation.h"
#import "SqrtOperation.h"
#import "InverseOperation.h"
#import "SinOperation.h"
#import "CosOperation.h"

@implementation CalculatorModel

-(instancetype)init{
    self=[super init];
    if(self) {
        _operations=@{ @"+":[[SumOperation alloc] init],
                       @"+/-":[[SignOperation alloc] init],
                       @"-":[[MinusOperation alloc] init],
                       @"=":[[EqualOperation alloc] init],
                       @"A/C":[[ClearOperation alloc] init],
                       @"C":[[ClearInputOperation alloc] init],
                       @"x":[[MultiOperation alloc] init],
                       @"÷":[[PartitionOperation alloc] init],
                       @"%":[[ProcentOperation alloc] init],
                       @"√x":[[SqrtOperation alloc] init],
                       @"cos":[[CosOperation alloc] init],
                       @"sin":[[SinOperation alloc] init],
                       @"1/x":[[InverseOperation alloc] init],
                       @"х²":[[SquareOperation alloc] init],

        };
    }
    return self;
}

-(id)operationForOperationString:(NSString *)operationString {
    id operation =[self.operations objectForKey:operationString];
    if(!operation){
        operation=[[UnaryOperation alloc] init];
    }
    return operation;
}

-(double)performOperation:(NSString *)operationString withValue:(double)value numberInput:(BOOL)numInput{
    
    id operation=[self.operations objectForKey:operationString];
    
    if ([operation isKindOfClass:[EqualOperation class]] ){
        if (self.currentBinaryOperation && [self.currentBinaryOperation isKindOfClass:[BinaryOperation class]]){
            if(numInput==NO )
            {
                if(self.currentBinaryOperation.lastOperand)
                value = [self.currentBinaryOperation performWithSecondArgument:self.currentBinaryOperation.lastOperand];
                else{
                    self.currentBinaryOperation.lastOperand=value;
                    value=[self.currentBinaryOperation performWithSecondArgument:self.currentBinaryOperation.firstArgument];
                    
                }
            }
            else {
                
                self.currentBinaryOperation.lastOperand=value;
                value = [self.currentBinaryOperation performWithSecondArgument:value];
            }
        }
        self.currentBinaryOperation.firstArgument=value;
    }
    
    if ([operation isKindOfClass:[UnaryOperation class]] ){
        return [operation perform:value];
    }

    if ([operation isKindOfClass:[ClearOperation class]] ){
        //self.currentBinaryOperation.firstArgument=0;
        self.currentBinaryOperation=nil;
        return [operation clearNumber:value];
    }
    
    if ([operation isKindOfClass:[BinaryOperation class]]){
        if(self.currentBinaryOperation && numInput==YES)
        {
            self.currentBinaryOperation.lastOperand=value;
            value=[self.currentBinaryOperation performWithSecondArgument:value ];
        }
        self.currentBinaryOperation=operation;
        self.currentBinaryOperation.firstArgument=value;
        
        return value;
    }
    return  DBL_MAX;
}
@end
