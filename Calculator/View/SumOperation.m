//
//  SumOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 09.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "SumOperation.h"

@implementation SumOperation

-(double)performWithSecondArgument:(double)secondArgument{
    return self.firstArgument+secondArgument;
}
@end
