//
//  CalculatorModel.h
//  Calculator
//
//  Created by Gorbenko Georgy on 09.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnaryOperation.h"
#import "BinaryOperation.h"

@interface CalculatorModel : NSObject

@property (nonatomic,strong) NSDictionary *operations;
@property (nonatomic,strong) BinaryOperation *currentBinaryOperation;
//@property (nonatomic,strong)

-(double)performOperation:(NSString *)operationString withValue:(double)value numberInput:(BOOL)numInput;

@end
