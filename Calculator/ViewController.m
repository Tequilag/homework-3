//
//  ViewController.m
//  Calculator
//
//  Created by Gorbenko Georgy on 08.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.model = [[CalculatorModel alloc] init];
}

- (IBAction)pressDigit:(UIButton *)sender {
    if (self.isNumberInputting){
        if (![sender.currentTitle isEqualToString:@"."] || ![self.numbericLabel.text containsString:@"."])
          {
              self.numbericLabel.text=[self.numbericLabel.text stringByAppendingString:sender.currentTitle];
          }
    }
    else {
        if ([sender.currentTitle isEqualToString:@"."]){
            self.numbericLabel.text= [NSString stringWithFormat:@"0."];
        }
        else{
            self.numbericLabel.text=sender.currentTitle;
        }
        self.numberInputting=YES;
    }
}

- (IBAction)performOperation:(UIButton *)sender {
    
    NSString *operationString = sender.currentTitle;
    //if (self.numberInputting==YES ||[operationString isEqualToString:@"="]){
    //if(![self.lastOperation isEqualToString:operationString]||[self.lastOperation isEqualToString:@"="]){
    
        double value =self.numbericLabel.text.doubleValue;
        double result =[self.model performOperation:operationString withValue:value numberInput:self.numberInputting];
        self.numberInputting=NO;
        //self.lastOperation=operationString;
        self.numbericLabel.text=[@(result) stringValue];
    //}
    //}
}
@end
