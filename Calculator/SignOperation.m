//
//  SignOperation.m
//  Calculator
//
//  Created by Gorbenko Georgy on 09.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "SignOperation.h"

@implementation SignOperation

-(double)perform:(double)value{
    return -value;
}
@end
