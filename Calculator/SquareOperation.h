//
//  SquareOperation.h
//  Calculator
//
//  Created by Gorbenko Georgy on 11.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "UnaryOperation.h"

@interface SquareOperation : UnaryOperation

@end
