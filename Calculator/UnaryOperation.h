//
//  UnaryOperation.h
//  Calculator
//
//  Created by Gorbenko Georgy on 09.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnaryOperation : NSObject

//@property(nonatomic,strong) NSString *lastOperand;

-(double)perform:(double)value;
@end
