//
//  ViewController.h
//  Calculator
//
//  Created by Gorbenko Georgy on 08.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorModel.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *numbericLabel;
@property (nonatomic,strong) CalculatorModel *model;
@property (nonatomic, assign, getter=isNumberInputting) BOOL numberInputting;
@property (nonatomic, strong) NSString *ButtonWait;

- (IBAction)pressDigit:(UIButton *)sender;

- (IBAction)performOperation:(UIButton *)sender;

@end

