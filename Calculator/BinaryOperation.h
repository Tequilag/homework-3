//
//  BinaryOperation.h
//  Calculator
//
//  Created by Gorbenko Georgy on 09.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BinaryOperation : NSObject

@property (nonatomic,assign) double firstArgument;
@property (nonatomic,assign) double lastOperand;
@property (nonatomic,strong) NSString *lastOperation;

-(double) performWithSecondArgument:(double)secondArgument;
@end
